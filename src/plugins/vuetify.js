// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'

export default createVuetify({
  theme: {
    defaultTheme: 'light',
    themes: {
      light: {
        colors: {
          primary: '#4CAF50', // первичный цвет
          secondary: '#394a58', // вторичный цвет
          surface: '#fff', // поверхность
          background: '#f8f8f8', // фон
          error: '#e21a1a',
          info: '#33d9ff',
          success: '#6bff26',
          warning: '#ff520f'
        }
      },
      dark: {
        colors: {
          primary: '#4CAF50' // первичный цвет
        }
      }
    }
  }
})
