import { useRouter } from 'vue-router'

export function useProducts() {
  const router = useRouter()

  function viewProductHandler(id) {
    router.push({ name: 'Detailed View', query: { id } })
  }

  return { viewProductHandler }
}
