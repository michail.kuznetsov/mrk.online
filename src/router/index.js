import { createRouter, createWebHistory } from 'vue-router'
import { defineAsyncComponent } from 'vue'
import store from '@/store'

const routes = [
  // Main Router
  {
    path: '/',
    meta: { layout: 'default' },
    name: 'Market Page',
    redirect: '/market'
  },
  // Market Views
  {
    path: '/market',
    meta: { layout: 'default', extended: 'marketMain', left: false },
    name: 'Маркет',
    component: () => import('@/views/MarketView.vue'),
    children: [
      {
        name: 'Create Product',
        path: 'create',
        meta: { auth: true, layout: 'extended', extended: 'marketCreate' },
        component: () => import('@/views/market/CreateProduct.vue')
      },
      {
        name: 'My Products',
        path: 'products',
        meta: { auth: true, layout: 'default' },
        component: () => import('@/views/market/CreateProduct.vue')
      }
    ]
  },
  {
    path: '/market/detail',
    meta: { layout: 'default' },
    name: 'Detailed View',
    component: () => import('@/views/market/ProductView.vue')
  },
  // Basket Views
  {
    path: '/basket',
    meta: { layout: 'default' },
    name: 'BasketView',
    component: () => import('@/views/BasketView.vue'),
    children: [
      {
        name: 'BasketCheck',
        path: 'check',
        meta: { layout: 'extended', extended: 'basketCheck' },
        component: () => import('@/views/basket/CheckView.vue')
      },
      {
        name: 'BasketOrder',
        path: 'order',
        meta: { layout: 'default' },
        component: () => import('@/views/basket/OrderView.vue')
      },
      {
        name: 'BasketPayment',
        path: 'payment',
        component: () => import('@/views/basket/PaymentView.vue')
      }
    ]
  },
  // Posts Views
  {
    path: '/posts/',
    meta: { layout: 'default', extended: 'postsActions' },
    name: 'Лента',
    component: () => import('@/views/PostsView.vue')
  },
  {
    path: '/posts/create',
    meta: { layout: 'default' },
    name: 'CreatePostsView',
    component: () => import('@/views/CreatePostsView.vue')
  },
  // Board Views
  {
    path: '/board',
    meta: { layout: 'default' },
    name: 'Доска объявлений',
    component: () => import('@/views/BoardView.vue')
  },
  // Help View
  {
    path: '/help',
    meta: { layout: 'default' },
    name: 'Помощь',
    component: () => import('@/views/HelpView.vue')
  },
  // Photos Views
  {
    path: '/photos',
    meta: { layout: 'default' },
    name: 'Фотография дня'
  }
]

const router = createRouter({
  mode: 'history',
  history: createWebHistory(process.env.BASE_URL),
  routes
})
router.beforeEach(async (to, from, next) => {
  const layoutComponentName = to.meta.layout ?? 'default'
  const extendedComponentName = to.meta.extended ?? false
  to.meta.layoutComponent = defineAsyncComponent(() =>
    import(`@/layouts/${layoutComponentName}/${layoutComponentName}Wrapper.vue`)
  )
  if (extendedComponentName) {
    to.meta.extendedComponent = defineAsyncComponent(() =>
      import(`@/components/extended/${extendedComponentName}Extended.vue`)
    )
  }
  if (to.meta?.auth === true) {
    console.log('auth barrier')
    console.log('redirect main path')
    const user = store.getters['user/isAuth'] ?? false
    user ? next() : next({ path: '/' })
  } else {
    console.log('no barrier')
    next()
  }
})

export default router
