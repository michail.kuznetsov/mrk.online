// Components
import App from './App.vue'
import vuetify from '@/plugins/vuetify'
import router from '@/router'
import store from '@/store'

// Composables
import { createApp } from 'vue'

// Plugins
import { registerPlugins } from '@/plugins'

const app = createApp(App)
// Добавьте глобальный метод для изменения темы и эффекта ripple
app.config.globalProperties.$toggleTheme = function () {
  vuetify.framework.theme.dark = !vuetify.framework.theme.dark
  const rippleElement = document.createElement('div')
  rippleElement.className = 'ripple-effect'
  document.body.appendChild(rippleElement)
  setTimeout(() => {
    document.body.removeChild(rippleElement)
  }, 600)
}

registerPlugins(app)
app.use(vuetify).use(router).use(store).mount('#app')
