import { make } from 'vuex-pathify'
import api from '@/services/api'

const state = () => ({
  products: [],
  createProduct: {
    image: 'https://metallps.ru/assets/cache_image/empty_720x540_e0a.png',
    price: 0,
    title: '',
    category: ''
  },
  searchField: '',
  order: {},
  basket: [],
  currentProduct: {},
  categories: [
    { name: 'Электроника', icon: 'mdi-laptop' },
    { name: 'Одежда и обувь', icon: 'mdi-tshirt-v-outline' },
    { name: 'Дом и сад', icon: 'mdi-sprout-outline' },
    { name: 'Детские товары', icon: 'mdi-baby-face-outline' },
    { name: 'Красота и здоровье', icon: 'mdi-bag-personal-outline' },
    { name: 'Бытовая техника', icon: 'mdi-washing-machine' },
    { name: 'Спорт и отдых', icon: 'mdi-basketball' },
    { name: 'Строительство и ремонт', icon: 'mdi-hammer' },
    { name: 'Продукты питания', icon: 'mdi-food-apple-outline' },
    { name: 'Аптека', icon: 'mdi-pill' },
    { name: 'Товары для животных', icon: 'mdi-dog-side' },
    { name: 'Книги', icon: 'mdi-book-open-page-variant-outline' },
    { name: 'Туризм рыбалка охота', icon: 'mdi-nature-people-outline' },
    { name: 'Автотовары', icon: 'mdi-car-wrench' },
    { name: 'Мебель', icon: 'mdi-sofa-single-outline' },
    { name: 'Хобби творчество', icon: 'mdi-palette' },
    { name: 'Ювелирные украшения', icon: 'mdi-diamond-stone' },
    { name: 'Аксессуары', icon: 'mdi-bow-tie' },
    { name: 'Икры и консоли', icon: 'mdi-controller' },
    { name: 'Канцелярские товары', icon: 'mdi-pin-outline' },
    {
      name: 'Антиквариат и коллекционирование',
      icon: 'mdi-treasure-chest-outline'
    },
    { name: 'Цифровые товары', icon: 'mdi-gesture-tap-button' },
    { name: 'Бытовая химия и гигиена', icon: 'mdi-spray-bottle' },
    { name: 'Музыка и видео', icon: 'mdi-music' },
    { name: 'Акции', icon: 'mdi-percent-outline' }
  ],
  currentTab: 'Market page'
})

const mutations = make.mutations(state)

const actions = {
  ...make.actions(state),
  getProducts({ state }) {
    const params = {
      category: 'Электроника'
    }
    api()
      .post('market/category/', params)
      .then(response => {
        state.products = response.data
      })
      .catch(error => {
        alert(error)
      })
  },
  getByCategory({ state }, category) {
    console.log(category)
    api()
      .post('getByCategory', { category: category })
      .then(response => {
        state.products = response.data
      })
      .catch(error => {
        alert(error)
      })
  },
  setSearchField({ state }, search) {
    state.searchField = search
  },
  addBasket({ state }, product) {
    state.basket.push(product)
  },
  deleteBasket({ state }, id) {
    const index = state.basket.findIndex(item => item.id === id)
    if (index !== -1) {
      state.basket.splice(index, 1)
    }
  },
  getProduct({ state }, id) {
    api()
      .get(`market/${id}`)
      .then(response => {
        state.currentProduct = {}
        state.currentProduct = response.data
      })
      .catch(error => {
        console.log(error)
      })
  },
  addProduct({ state }) {
    api()
      .post('market/add', state.createProduct)
      .then(product => {
        state.products.push(product.data)
      })
      .catch(error => {
        alert(error)
      })
  }
}

const getters = {
  filteredCards(state) {
    return state.products.filter(product => {
      return product.title
        .toLowerCase()
        .includes(state.searchField.toLowerCase())
    })
  },
  basketTotalCount(state) {
    return state.basket.length
  },
  basketTotalPrice(state) {
    let totalPrice = 0
    for (let i = 0; i < state.basket.length; i++) {
      totalPrice += state.basket[i].price
    }
    return totalPrice
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
