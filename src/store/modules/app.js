import { make } from 'vuex-pathify'

const state = () => ({
  drawer: true,
  mini: true,
  currentNav: '',
  navItems: [
    { title: 'МРК - Маркет', icon: 'mdi-shopping-outline', value: 'market' },
    {
      title: 'Лента',
      icon: 'mdi-arrow-up-down-bold-outline',
      value: 'posts'
    },
    {
      title: 'Объявления',
      icon: 'mdi-newspaper-variant-outline',
      value: 'board'
    },
    { title: 'Фотография дня', icon: 'mdi-camera-outline', value: 'photos' },
    {
      title: 'Информация',
      icon: 'mdi-comment-question',
      value: 'help'
    }
  ]
})

const mutations = make.mutations(state)

const actions = {
  ...make.actions(state),
  init() {
    console.log('init action')
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
