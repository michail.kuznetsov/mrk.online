import { make } from 'vuex-pathify'
import api from '@/services/api'

const state = () => ({
  feeds: [],
  newFeed: {
    title: '',
    author: '',
    date: 0,
    content: '',
    authorContent: true,
    nsfw: false,
    noComments: false,
    anon: false
  },
  searchField: '',
  currentFeed: {},
  categories: [
    'Бытовая технкиа',
    'Все для дома',
    'Компьютеры',
    'Одежда',
    'Текстиль',
    'другое'
  ],
  currentTab: 'Market page'
})

const mutations = make.mutations(state)

const actions = {
  ...make.actions(state),
  getProducts({ state }) {
    api()
      .get('market/1')
      .then(response => {
        state.products = response.data
      })
      .catch(error => {
        alert(error)
      })
  },
  setSearchField({ state }, search) {
    state.searchField = search
  },
  addBasket({ state }, product) {
    state.basket.push(product)
  },
  deleteBasket({ state }, id) {
    const index = state.basket.findIndex(item => item.id === id)
    if (index !== -1) {
      state.basket.splice(index, 1)
    }
  },
  getProduct({ state }, id) {
    api()
      .get(`market/${id}`)
      .then(response => {
        state.currentProduct = {}
        state.currentProduct = response.data
      })
      .catch(error => {
        console.log(error)
      })
  },
  addProduct({ state }) {
    api()
      .post('market/add', state.createProduct)
      .then(product => {
        state.products.push(product.data)
      })
      .catch(error => {
        alert(error)
      })
  }
}

const getters = {
  filteredCards(state) {
    return state.products.filter(product => {
      return product.title
        .toLowerCase()
        .includes(state.searchField.toLowerCase())
    })
  },
  basketTotalCount(state) {
    return state.basket.length
  },
  basketTotalPrice(state) {
    let totalPrice = 0
    for (let i = 0; i < state.basket.length; i++) {
      totalPrice += state.basket[i].price
    }
    return totalPrice
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
