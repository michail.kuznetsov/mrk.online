import { make } from 'vuex-pathify'
import api from '@/services/api'
import router from '@/router'

const state = () => ({
  user: {}
})

const mutations = make.mutations(state)

const actions = {
  ...make.actions(state),
  login({ state }, user) {
    api()
      .post('auth/login', user)
      .then(response => {
        state.user = response.data
        console.log(777)
      })
      .catch(error => {
        alert(error)
      })
  },
  logout({ state }, user) {
    api()
      .post('auth/login', user)
      .then(() => {
        state.user = {}
        router.push({ path: '/' })
        console.log('logout')
      })
      .catch(error => {
        alert(error)
      })
  },
  // get user from local storage
  fetch: ({ commit }) => {
    const local = localStorage.getItem('ocrv@user') || '{}'
    const user = JSON.parse(local)

    for (const key in user) {
      commit(key, user[key])
    }

    // if (user.dark === undefined) {
    //   commit('dark', window.matchMedia('(prefers-color-scheme: dark)'))
    // }

    // set jwt in axios headers
    // if (user.userProfile && user.userProfile.token) {
    //   console.log('GET user to localStorage')
    //   console.log('JWT:' + state.userProfile.token)
    //   axios.defaults.headers.common.Authorization = state.userProfile.token
    //   // axios.defaults.headers.common.Authorization = window.api.FABRIC_JWT
    // }
  },

  update: ({ state }) => {
    console.log('SET user to localstorage')
    // axios.defaults.headers.common.Authorization = state.userProfile.token
    localStorage.setItem('mrk@user', JSON.stringify(state.user))
  }
}

const getters = {
  isAuth(state) {
    return true
    // return !!state.user.username
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
