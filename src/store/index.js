import { createStore } from 'vuex'
import pathify from 'vuex-pathify'
import * as modules from '@/store/modules'

const store = createStore({
  plugins: [pathify.plugin],
  modules
})

store.dispatch('app/init')

export default store
